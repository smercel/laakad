# Mission 1 : Découverte de la simulation réseau avec Filius
## Jeudi 23 septembre 2021 
## par MAVALY Kameshdassane et LAAKAD Yanis

## Mission 1 

### Etape 1

* Question 1: Quel est le masque de sous-réseau de chaque machine ?

	**1111 1111. 1111 1111. 0000 0000. 0000 0000**
	**Le masque sous-reseau est 255.255.0.0 (ou /16 en notation CIDR).**

	**1111 1111. 1111 1111. 1111 0000. 0000 0000**
	**Le masque sous-reseau est 255.255.240.0 (ou /20 en notation CIDR).**

* Question 2: A quel sous-réseau IP appartient chaque machine ? Justifier.


	**L'adresse réseau est 192.168.0.0/16 ou /20** 

justification:M1,M2 et M3      
			    192   .   168   .   10    .   10    

			1111 1111.1111 1111.0000 0000.0000 0000

		  	    192   .   168   .    0    .    0



	   **Nous voyons bien que l'adresse réseau est 192.168.0.0**

              
* Question 3: Peuvent-elles potentiellement communiquer entre elles ? Justifier.


	**La machine 1 et 2 peuvent communiquer ensemble car ils n'ont pas la meme adresse IP**
mais la M2 et la M3 ne peuvent pas communiquer ensemble car ils ont les mêmes adresses IP.**

* Question 4: Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.


	**Non, car elle ne sont pas relier entre elle par un switch de plus elle ne sont pas connecter au réseau.** 

* Question 5: Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA ?


	**La commande est "ipconfig"**

* Question 6: Quelle commande permet de tester que les messages IP sont bien transmis entre deux machines ?


	**La commande est "ping (adresse ip de la machine a contacter)"**



### Etape 2 

* Question 1: Quel média de communication a été ajouté ? Expliquer brièvement son fonctionnement.


	**Le media de communication ajouté est un commutateur(switch).**


 **un switch est un appareil qui perrmet de faire communiquer des ordinateurs entre eux dans un réseau.**

* Question 2: Quel autre média aurait pu le remplacer (inexistant sur Filius) ? En quoi est-il différent du premier ?


	**Il y a le concentrateur(HUB), cette machine sert comme point de connexion commun pour les périphériques d'un réseau.**

* Question 3: Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.


	**Non, les machines ne peuvent pas communiquer entre elles car la machine M2 et M3 ont la même adresse IP..**



* Question 4: Si nécessaire, corriger les configurations pour permettre à toutes les STAs de communiquer entre elles. Quelles sont les modifications 		      apportées ?

    	      **Les modifications a faire son de changer l'adresse IP de la M2 par exemple: 192.168.0.11 et celle de la M3 par example:192.168.11.1.**


		**cela est la même chose pour les deux cas c'est pour le /16 et /20**

* Question 5: Si Machine 1 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse logique peut-elle utiliser ?


	**Si la Machine 1 veut communiquer elle va utiliser l'adresse de diffusion qui est 192.168.255.255 pour le /16 et 192.168.15.255 pour le /20.**




### Etape 3 

* Question 1: A quel réseau appartient la 4e machine (M4) ? Est-ce le même que celui des Machines M1, M2 et M3 ? Justifier.


	**La Machine M4 n'appartient pas au même reseau car l'adresse IP est 192.169.0.10 et pas 192.168.0.10**

* Question 2: M4 peut-elle communiquer avec M1, M2 ou M3 ? Justifier.


	**Non elle ne peut pas communiquer car la machine 4 n'est pas sur le même reseau que le la M1,M2,M3.**

* Question 3: Si nécessaire, reconfigurer M2 pour lui permettre d’échanger des messages avec M4. Quels sont les changements nécessaires ?


	**Il faut remplacer l'adresse IP de la machine M2 qui est 192.168.0.10 par 192.169.0.13.**

* Question 4: Est-ce que M1 et M2 peuvent toujours communiquer ? Justifier.


	**Non il ne peuvent plus communiquer car ils ne sont plus sur le même reseau.**







