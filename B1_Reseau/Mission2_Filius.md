# Mission2 : Simulation Filius d'échanges entre deux sous-réseaux logiques 
## 21/10/2021
### Musa TURNA, Yanis LAAKAD







Etape 1 

**Question 1** 
Quel est le CIDR de chaque machine ? Justifier. 

Le CIDR de chaque machines est /16 car leur masque de sous réseaux est : 255.255.0.0 ce qui fait 16 bits. 

**Question 2** 
A quel sous-réseau IP appartient chacune de ces 4 machines ? Justifier. 

M1 : 10.10.0.0
M2 :10.0.0.0
M3 :10.0.0.0 
M4 : 10.10.0.0

**Question3** 
Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés ? 

Le nombre de STAs maximum est de  65534
                                                             =2^(32-16)-2
                                                             =65534 STAs

**Question 4** 
Quelles machines peuvent potentiellement communiquer entre elles ? Justifier. 	

Les machines M1 et M3, peuvent communiquer entre elles car elles sont sur le même réseau et la machine M4 ne peut pas communiquer avec les machines M1,M2,M3 car elle n’est pas sur le même reseau. L’adresse IP de M2 finit par 255 donc il ne peut pas communiquer avec les autres car l’adresse du broadcast

**Question5** 
Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier. 

Les machines M1,M2 et M3 peuvent communiquer entres elles car ils sont sur le même reseau tandis que la machine M4 fait parti d’un autre réseau.  

**Question6** 
Quelle est la commande complète qui permet à M1 de tester la liaison avec M2 

C’est la commande « ping 10.0.1.255 » qui permet de tester la liaison avec la machine M2.










Etape 2 

**Question1**
Quelles sont les adresses IP possibles pour M5 et M6 ? 

Pour que M5 puisse communiquer avec M1 il doit utiliser une adresse entre : 10.10.0.1/16 (adresse minimale) jusqu’à 10.10.255.254/16 (adresse maximale), sauf l’adresse IP de M1 et M4.
Pour que M6 puisse communiquer avec M2 il doit utiliser une adresse entre : 10.0.0.1/16 (adresse minimale) jusqu’à 10.0.255.254/16 (adresse maximale), sauf l’adresse IP de M2 et M3.

**Question2**
Combien de machines peut-on encore ajouter dans chaque sous-réseau ? 

Pour le réseau 1 qui contient M1,M4 et M5
  =2^(32-16)-4
 =65532 STAs
Pour ce réseau il reste 65532 adresses IP utilisable. 

Pour le réseau 2 qui contient M2,M3 et M6 
=2^(32-16)-4
=65532 STAs 
Pour ce réseau il reste encore 65532 adresses IP utilisable.

**Question 3**
Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser ? 

Si M6 veut envoyer un message à toutes les machines de son sous-réseau, il doit utiliser l’adresse IP broadcast c’est à dire 10.0.255.255 

**Question 4**
Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages ? 

Un routeur est nécessaire pour permettre à toutes les machines d ‘échanger des messages. 






Etape 3 

**Question1**
Expliquer brièvement le fonctionnement d’un routeur.

Le switch et le routeur se ressemble à la seule différence que un routeur permet la communication entre les ordinateurs connectés au routeur et à Internet, alors qu’un switch permet de connecter plusieurs appareils dans un même réseau.

**Question2** 
Compléter la configuration physique pour permettre aux différentes machines des différents réseaux logiques de communiquer. 
    • Combien d’interfaces réseaux sont nécessaires ? 
    • Quelle adresse IP aura chaque interface ?

Deux interfaces réseaux sont nécessaires, car nous avons que deux réseaux logiques. 
Le premier interface est connecté au réseau logique en 10.0.0.0 avec l’adresse IP 10.0.255.254
Le deuxième interface est connecté au réseau logique en 10.10.0.0 avec l’adresse IP 10.10.255.254 

**Question3**
Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier. 

Oui elles communiquent car on a réussit à vérifier la communication avec la commande ping. 

**Question4**
Quelle nouvelle configuration est nécessaire pour permettre aux machines de communiquer avec des machines appartenant à d’autres réseaux (M5 avec M6 par exemple)? 
On peut utiliser ce protocole réseau ad hoc ou WANET ou encore MANET, qui permet aux ordinateurs de communiquer directement entre eux sans routeur. 

