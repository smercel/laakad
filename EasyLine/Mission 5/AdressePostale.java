public class AdressePostale { 

    private  String adresse;
    private  String ville;
    private  String cp; 
    

    public AdressePostale(){
    }
    public AdressePostale(String adresse,String ville,String cp){
            this.adresse=adresse;
            this.ville=ville;
            this.cp=cp;
    }   
        public void setAdresse(String adresseV){
            this. adresse = adresseV;
        } 
        public void setVille(String villeV){
            this.ville = villeV;
        } 
        public void setCp(String cpV){
            this.cp=cpV;
        }
        public  String getAdresse()
        {
        return this.adresse;
        } 
        public String getVille()
        {
        return this.ville;
        } 
        public String getCp()
        {
          return this.cp;
        }
        public void afficher(){
            System.out.println(this.adresse);
            System.out.println(this.ville);
            System.out.println(this.cp);
        }
        public String toString() { return "Votre adresse est : " + this.adresse +" " + "a" +" " + this.ville +" "+  "dans le "+ this.cp;}
    
    
}