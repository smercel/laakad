import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Array;
import java.util.Scanner; 

public class AgenceVoyage{

    private  String nomagence;
    private  AdressePostale adresse;
    private  ArrayList<Voyageur> vlist = new ArrayList();
    public  AgenceVoyage(String nomagenceV, AdressePostale adresseV){

        this.nomagence=nomagenceV;
        this.adresse=adresseV;

        this.vlist.add(new Voyageur("Luc",19));
        this.vlist.add(new Voyageur("Marc",9));
        this.vlist.add(new Voyageur("Silvie",15));
        this.vlist.add(new Voyageur("Loic",12));
        this.vlist.add(new Voyageur("Mathias",23));

    }

    public AgenceVoyage(String nomagence, AdressePostale adresse, ArrayList<Voyageur> vlist) {

        this.nomagence=nomagence;
        this.adresse=adresse;
        this.vlist=vlist;

    }
    public String getNomagence(){
        return nomagence;
    }
    public AdressePostale getAdresse(){
        return adresse;
        
    }
    public ArrayList<Voyageur> getvlist(){
        return this.vlist;
    }
    public void setNomagence(String nomagenceV){
        this.nomagence =nomagenceV;
    }
    public void setAdresse(AdressePostale adresseV){
        this.adresse=adresseV;
    }
 

    
}
