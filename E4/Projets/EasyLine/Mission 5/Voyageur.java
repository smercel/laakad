public class Voyageur {
    
    // On modifie le "private " en "protected" 
    protected  String nom;
    protected  int age;
    protected AdressePostale adresse;
    protected Bagage bagage;
 
    public Voyageur(){
    }
    public Voyageur(String nom,int age){
        this.nom=nom;
        this.age=age;
    }
    public void setAge(int ageV){
        this.age=ageV;
    } 
    public void setNom(String nomV){
        this.nom=nomV;
    } 
    public  int getAge()
    {
    return this.age;
    } 
    public String getNom()
    {
    return this.nom;
    }
    public void afficher(){
        System.out.println(this.age);
        System.out.println(this.nom);
    }
    public String toString() { return "Age " + this.age + " Nom " + this.nom + "Bagage " + this.bagage + " adresse" + this.adresse;
     }




}

    

